# Development Journal

## April 24, 2024
---
- Session Goals:
  - Implement CardType Enum.
  - Implement Card Class.
  - Implement CardPile Class.
  - Implement Deck Class.
  - Start testing CardPile Class.

## April 26, 2024
---
- Session Goals:
  - Finish testing CardPile Class.
  - Implement Player Class.
  - Test Player Class.
  - Start to Implement GameManager Class.

- Last Session Achievments:
  - Implement CardType Enum.
  - Implement Card Class.
  - Implement CardPile Class.
  - Implement Deck Class.
  - Figured out how to initialize the deck constructor.

## April 28, 2024
---
- Session Goals:
  - Implement more of the game loop.
  - Rotate moving hands between players.

- Last Session Achievements:
  - Sucessfully started implementing the game loop.
    - Begin turn and perform turn methods.
    - Figured out the logic necessary to perform the game loop.
  - Formatted the game loop nicely with colors.
  - Developement the UI for the game loop.

## May 1, 2024
---
- Session Goals:
  - Expand comments and clean up and organize code more.
  - Begin implementing the score calculation methods .
- Last Session Achievements:
  - Implemented the bulk of the of game loop. Minus the calculation of score and end game.
  - Sucessfully implemented the rotation of moving hands between turns.
  - Implemented the fork card to allow a player to choose two cards.
  - Implemented a checkRoundOver function that ensures all players have no remaining cards.

## May 2, 2024
---
- Session Goals:
  - Implement the calculatePoints method to calculate player points at end of round.
- Last Session Achievements:
  - Expanded on the comments within the program to be more insightful.
  - Outlined the calculatePoints function with comments.
  - Fixed a bug with the game loop.

## May 3, 2024
---
- Session Goals:
  - Implement try catch exception handling to ensure game doesn't crash.
  - Refactor any code necessary.
  - Flesh out comments to ensure they provide as much info as possible.
- Last Session Achievements:
 - Implemented the calculatePoints method and all of the necessary helper methods.
 - Finished the game loop by adding an endGame method.

## May 6, 2024
---
- Session Goals:
  - Test the game loop and ensure no bugs occur.
  - Finish expanding on comments for readability.
- Last Session Achievements:
  - Implemented try catch exception handling for all possible exceptions.
  - Fleshed out comments more.
  - Added fun deck shuffle intro for users.
  - Added funny obedience check for the end game.

## May 8, 2024
---
- Session Goals:
  - Refactor code to make it more readable.
- Last Session Achievements:
  - Found bug in 5 player game loop and fixed it.
  - Expanded all comments to make sure they're informative.

## May 9, 2024
---
- Session Goals:
  - Refactor more.
  - Add one line comments to clarify logic.
- Last Session Achievements:
 - Did a huge refactor when I realized that I could encapsulate Player actions.
 - Added variables to methods for readability.
 - Expanded on comments I forgot to expand on.

## May 10, 2024
---
- Session Goals:
  - Double check everything works well on school computer.
  - Test conditions for bugs.
  - Refactor further.
- Last Session Achievements:
  - Refactored more methods by adding variables and comments.
  - Added extra shuffle (to avoid photographic memory cheaters).

## May 11, 2024
---
- Session Goals:
  - Double check everything.
  - Fill out self assessment.
- Last Session Achievements:
  - Ensured that the game looks good on the school computer.
  - Tested for more bugs, fixed a bug noticed with cupcake counting.
  - Refactored comments and reordered GameManager methods.
  - Refactored Player class to only have userinput in the GameManager class.

## May 12, 2024
---
- Session Goals:
  - Submit.
- Last Session Acheivements:
  - Double checked everything!
  - Filled out self assessment.

## End
- Woohoo!!